//
//  Unwrapable.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation
@testable import Survey_iOS
import XCTest

/*
 * COPY - PASTED
 */

// MARK: - PropertyWrapper

@propertyWrapper
struct Unwrapable<Value> {
    private(set) var value: Value?

    var wrappedValue: Value? {
        get { value }
        set { value = newValue }
    }

    func safeValue(
        file: StaticString = #file,
        line: UInt = #line,
        function: StaticString = #function
    ) -> Value! {
        guard let someValue = value else {
            XCTFail("'\(function)' function with signature '\(Value.self)' called but not stubbed before.", file: file, line: line)
            fatalError("Should be used in combination with continueAfterFailure = false")
        }
        return someValue
    }

    var projectedValue: Unwrapable<Value> { return self }
}
