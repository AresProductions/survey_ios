//
//  BaseTestCase.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
import Foundation
@testable import Survey_iOS
import XCTest

class BaseTestCase: XCTestCase {
    override func setUp() {
        continueAfterFailure = false
        AppServices.shared = AppServices(testing: true, empty: true)
    }

    override class func tearDown() {
        AppServices.shared = AppServices(testing: true, empty: true)
    }
}

protocol ReferenceCount {
    static var referenceCount: Int { get }
}
