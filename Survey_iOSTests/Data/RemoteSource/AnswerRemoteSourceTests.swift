//
//  AnswerRemoteSourceTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
import XCTest
@testable import Survey_iOS

class AnswerRemoteSourceTests: BaseTestCase {
    func arrange() -> (source: AnswerRemoteSource,
                       restClient: RestClientMock) {
        let mock = RestClientMock()
        let di = AppServices(testing: true, empty: true)
            .register(IRestClient.self) { _ in mock }
        AppServices.shared = di
        return (AnswerRemoteSource(), mock)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IAnswerRemoteSource.self))
    }
    
    func testPost() {
        let r = arrange()
        let answer = Answer(id: 0, description: "")
        r.restClient.stub.post = returnSuccess { }
        waitForSuccessfulCompletion(of: r.source.post(answer: answer))

        XCTAssertEqual(r.restClient.verify.post, [SurveyApiEndpoint.questionSubmit.path])
    }
}
