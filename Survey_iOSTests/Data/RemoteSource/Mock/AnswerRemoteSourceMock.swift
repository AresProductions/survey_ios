//
//  AnswerRemoteSourceMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class AnswerRemoteSourceMock: IAnswerRemoteSource {
    class Stub {
        @Unwrapable var post: ((Answer) -> AnyPublisher<Answer, Error>)?
    }

    class Verify {
        var post: [Answer] = []
    }

    public let stub = Stub()
    public let verify = Verify()

    func post(answer: Answer) -> AnyPublisher<Answer, Error> {
        verify.post.append(answer)
        return stub.$post.safeValue()(answer)
    }
}
