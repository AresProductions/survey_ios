//
//  QuestionLocalSourceMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS

class QuestionLocalSourceMock: IQuestionLocalSource {
    class Stub {
        @Unwrapable var getAll: (() -> AnyPublisher<[Question], Never>)?
        @Unwrapable var get: (() -> AnyPublisher<Question?, Never>)?
        @Unwrapable var createOrUpdate: (() -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var getAll: [Void] = []
        var get: [Int] = []
        var createOrUpdate: [[Question]] = []
    }

    let stub = Stub()
    let verify = Verify()

    func getAll() -> AnyPublisher<[Question], Never> {
        verify.getAll.append(())
        return stub.$getAll.safeValue()()
    }

    func get(id: Int) -> AnyPublisher<Question?, Never> {
        verify.get.append(id)
        return stub.$get.safeValue()()
    }

    func createOrUpdate(questions: [Question]) -> AnyPublisher<Void, Error> {
        verify.createOrUpdate.append(questions)
        return stub.$createOrUpdate.safeValue()()
    }
}
