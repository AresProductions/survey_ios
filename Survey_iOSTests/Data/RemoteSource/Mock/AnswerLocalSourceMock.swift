//
//  AnswerLocalSourceMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS

class AnswerLocalSourceMock: IAnswerLocalSource {
    class Stub {
        @Unwrapable var getAll: (() -> AnyPublisher<[Answer], Never>)?
        @Unwrapable var get: (() -> AnyPublisher<Answer?, Never>)?
        @Unwrapable var createOrUpdate: (() -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var getAll: [Void] = []
        var get: [Int] = []
        var createOrUpdate: [Answer] = []
    }

    let stub = Stub()
    let verify = Verify()

    func getAll() -> AnyPublisher<[Answer], Never> {
        verify.getAll.append(())
        return stub.$getAll.safeValue()()
    }

    func get(id: Int) -> AnyPublisher<Answer?, Never> {
        verify.get.append(id)
        return stub.$get.safeValue()()
    }

    func createOrUpdate(answer: Answer) -> AnyPublisher<Void, Error> {
        verify.createOrUpdate.append(answer)
        return stub.$createOrUpdate.safeValue()()
    }
}
