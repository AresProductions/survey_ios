//
//  QuestionRemoteSourceMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class QuestionRemoteSourceMock: IQuestionRemoteSource {
    class Stub {
        @Unwrapable var getAll: (() -> AnyPublisher<[Question], Error>)?
    }

    class Verify {
        var getAll: [Void] = []
    }

    public let stub = Stub()
    public let verify = Verify()

    func getAll() -> AnyPublisher<[Question], Error> {
        verify.getAll.append(())
        return stub.$getAll.safeValue()()
    }
}
