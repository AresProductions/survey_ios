//
//  RestClientMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
import XCTest
@testable import Survey_iOS

class RestClientMock: IRestClient {
    class Stub {
        @Unwrapable var get: (() -> AnyPublisher<AnyObject, Error>)?
        @Unwrapable var post: (() -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var get: [String] = []
        var post: [String] = []
    }

    let stub = Stub()
    let verify = Verify()

    func get<T, S>(_ endpoint: S) -> AnyPublisher<T, Error> where T: Decodable, S: Endpoint {
        verify.get.append(endpoint.path)
        return stub.$get.safeValue()().map { return $0 as! T }.eraseToAnyPublisher()
    }

    func post<S, E>(_ endpoint: E, using _: S) -> AnyPublisher<Void, Error> where S: Encodable, E: Endpoint {
        verify.post.append(endpoint.path)
        return stub.$post.safeValue()().eraseToAnyPublisher()
    }

}

enum SomeError: Error {
    case error
}
