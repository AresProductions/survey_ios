//
//  RestClientTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import XCTest
@testable import Survey_iOS

class RestClientTests: BaseTestCase {
    private func arrange() -> (restClient: RestClient,
                               notificationCenter: NotificationCenter,
                               sessionConf: URLSessionConfiguration) {
        let sessionConfig = URLSessionConfiguration.ephemeral
        sessionConfig.protocolClasses = [URLProtocolMock.self]

        let notificationCenter = NotificationCenter()

        let client = RestClient(sessionConfig: sessionConfig)
        return (client, notificationCenter, sessionConfig)
    }

    func testError400() {
        let r = arrange()
        let mURL = MethodURL(method: "GET", url: SurveyApiEndpoint.questions.url.absoluteString)
        URLProtocolMock.testURLs[mURL] = MockResponse(code: 400)
        let result: AnyPublisher<String, Error> = r.restClient.get(SurveyApiEndpoint.questions)
        waitForFailureCompletion(of: result)
    }
    
    func testGet() {
        let r = arrange()
        let mURL = MethodURL(method: "GET", url: SurveyApiEndpoint.questions.url.absoluteString)
        URLProtocolMock.testURLs[mURL] = MockResponse.mock
        let result: AnyPublisher<String, Error> = r.restClient.get(SurveyApiEndpoint.questions)
        waitForSuccessfulCompletion(of: result)
    }
    
    func testPost() {
        let r = arrange()
        let mURL = MethodURL(method: "POST", url: SurveyApiEndpoint.questions.url.absoluteString)
        URLProtocolMock.testURLs[mURL] = MockResponse.mock
        let result: AnyPublisher<Void, Error> = r.restClient.post(SurveyApiEndpoint.questions,
                                                                    using: "body")
        waitForSuccessfulCompletion(of: result)
    }

}

private struct MockResponse {
    let code: Int
    let data: Data

    init(code: Int, data: Data = Data()) {
        self.code = code
        self.data = data
    }

    static var mock: MockResponse {
        MockResponse(code: 200,
                     data: "\"mockData\"".data(using: .utf8)!)
    }
}

private struct MethodURL: Hashable {
    let method: String
    let url: String
}

private class URLProtocolMock: URLProtocol {
    static var testURLs: [MethodURL: MockResponse] = [:]
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func startLoading() {
        if let url = request.url,
            let method = request.httpMethod {
            let methodUrl = MethodURL(method: method, url: url.absoluteString)
            if let data = URLProtocolMock.testURLs[methodUrl] {
                let response = HTTPURLResponse(url: url,
                                               statusCode: data.code,
                                               httpVersion: nil,
                                               headerFields: nil)!

                client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
                client?.urlProtocol(self, didLoad: data.data)
            }
        }

        client?.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() {}
}
