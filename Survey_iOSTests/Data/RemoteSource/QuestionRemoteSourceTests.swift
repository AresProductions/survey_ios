//
//  QuestionRemoteSourceTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class QuestionRemoteSourceTests: BaseTestCase {
    func arrange() -> (source: QuestionRemoteSource,
                       restClient: RestClientMock)
    {
        let mock = RestClientMock()
        let di = AppServices(testing: true, empty: true)
            .register(IRestClient.self) { _ in mock }
        AppServices.shared = di
        return (QuestionRemoteSource(), mock)
    }
    
    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IQuestionRemoteSource.self))
    }

    func testGet() {
        let r = arrange()
        let questions = [RQuestion(id: 0, question: "")]
        r.restClient.stub.get = returnSuccess { questions }
        waitForSuccessfulCompletion(of: r.source.getAll())

        XCTAssertEqual(r.restClient.verify.get, [SurveyApiEndpoint.questions.path])
    }
}
