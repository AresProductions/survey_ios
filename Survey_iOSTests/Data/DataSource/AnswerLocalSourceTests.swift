//
//  AnswerLocalSourceTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
import CoreData
@testable import Survey_iOS
import XCTest

class AnswerLocalSourceTests: BaseTestCase {
    private func arrange() -> (source: AnswerLocalSource, container: NSPersistentContainer) {
        let container = NSPersistentContainer.createForSurveys(testing: true)
        AppServices.shared = AppServices(testing: true, empty: true)
            .register(NSPersistentContainer.self) { _ in container }
        return (AnswerLocalSource(), container)
    }

    func testAppServices() {
        XCTAssertNotNil(AppServices(testing: true).resolve(IAnswerLocalSource.self))
    }

    func testGetAndCreate() throws {
        // given
        let r = arrange()
        let answer = Answer(id: 0, description: "")

        // when
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(answer: answer))

        // then
        waitForValue(of: r.source.get(id: answer.id), value: answer)
    }

    func testUpdate() throws {
        // given
        let r = arrange()
        let answer = Answer(id: 0, description: "updated")
        r.container.update(blocking: true) { context in
            let dAnswer = DAnswer(context: context)
            dAnswer.id = Int16(answer.id)
            dAnswer.answer = "something else"
            try context.saveIfChanged()
        }

        // when
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(answer: answer))

        // then
        waitForValue(of: r.source.get(id: answer.id), value: answer)
    }

    func testGetAll() throws {
        // given
        let r = arrange()
        let answer = Answer(id: 0, description: "")

        // when
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(answer: answer))
        let answer2 = Answer(id: 2, description: "")
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(answer: answer2))

        // then
        waitForValue(of: r.source.getAll(), value: [answer, answer2])
    }
}
