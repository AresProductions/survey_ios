//
//  QuestionLocalSourceTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
import CoreData
@testable import Survey_iOS
import XCTest

class QuestionLocalSourceTests: BaseTestCase {
    private func arrange() -> (source: QuestionLocalSource, container: NSPersistentContainer) {
        let container = NSPersistentContainer.createForSurveys(testing: true)
        AppServices.shared = AppServices(testing: true, empty: true)
            .register(NSPersistentContainer.self) { _ in container }
        return (QuestionLocalSource(), container)
    }

    func testAppServices() {
        XCTAssertNotNil(AppServices(testing: true).resolve(IQuestionLocalSource.self))
    }

    func testGetAndCreate() throws {
        // given
        let r = arrange()
        let questions = [Question(id: 0, description: ""), Question(id: 1, description: "")]

        // when
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(questions: questions))

        // then
        waitForValue(of: r.source.get(id: questions[0].id), value: questions[0])
    }

    func testUpdate() throws {
        // given
        let r = arrange()
        let questions = [Question(id: 0, description: "updated")]
        r.container.update(blocking: true) { context in
            let dQuestion = DQuestion(context: context)
            dQuestion.id = Int16(questions[0].id)
            dQuestion.question = "something else"
            try context.saveIfChanged()
        }

        // when
        waitForSuccessfulCompletion(of: r.source.createOrUpdate(questions: questions))

        // then
        waitForValue(of: r.source.get(id: questions[0].id), value: questions[0])
    }

    func testLocalGetAll() throws {
        // given
        let r = arrange()
        let questions = [Question(id: 0, description: ""), Question(id: 1, description: "")]

        // when
        r.container.update(blocking: true) { context in
            let dQuestion = DQuestion(context: context)
            dQuestion.id = Int16(questions[0].id)
            dQuestion.question = questions[0].description
            
            let dQuestion2 = DQuestion(context: context)
            dQuestion2.id = Int16(questions[1].id)
            dQuestion2.question = questions[1].description
            try context.saveIfChanged()
        }

        // then
        waitForValue(of: r.source.getAll(), value: questions)
    }
}
