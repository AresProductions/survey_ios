//
//  QuestionRepoTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class QuestionRepoTests: BaseTestCase {
    private func arrange() -> (repo: QuestionRepo,
                               localSource: QuestionLocalSourceMock,
                               remoteSource: QuestionRemoteSourceMock)
    {
        let local = QuestionLocalSourceMock()
        let remote = QuestionRemoteSourceMock()

        let di = AppServices(testing: true, empty: true)
            .register(IQuestionLocalSource.self) { _ in local }
            .register(IQuestionRemoteSource.self) { _ in remote }
        AppServices.shared = di

        return (QuestionRepo(), local, remote)
    }

    func testAppServices() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IQuestionRepo.self))
    }

    func testGetAll() {
        let r = arrange()
        let questions = [Question(id: 1, description: "1"), Question(id: 2, description: "2")]
        r.localSource.stub.getAll = returnSuccess { questions }
        waitForSuccessfulCompletion(of: r.repo.getAll())
        XCTAssertEqual(r.localSource.verify.getAll.count, 1)
    }

    func testGet() {
        let r = arrange()
        let question = Question(id: 2, description: "2")
        r.localSource.stub.get = returnSuccess { question }
        waitForSuccessfulCompletion(of: r.repo.get(id: 2))
        XCTAssertEqual(r.localSource.verify.get, [2])
    }

    func testSync() {
        let r = arrange()
        let questions = [Question(id: 1, description: "description"), Question(id: 2, description: "description 2")]

        r.localSource.stub.createOrUpdate = returnSuccess {}
        r.remoteSource.stub.getAll = returnSuccess { questions }

        waitForSuccessfulCompletion(of: r.repo.sync())
    }
    
    func testSyncFailure() {
        let r = arrange()

        r.localSource.stub.createOrUpdate = returnSuccess {}
        r.remoteSource.stub.getAll = returnError { RestClientErrors.requestFailed(code: 400) }

        waitForFailureCompletion(of: r.repo.sync())
    }
}
