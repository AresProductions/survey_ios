//
//  AnswerRepoTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 6/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class AnswerRepoTests: BaseTestCase {
    private func arrange() -> (repo: AnswerRepo,
                               localSource: AnswerLocalSourceMock,
                               remoteSource: AnswerRemoteSourceMock)
    {
        let local = AnswerLocalSourceMock()
        let remote = AnswerRemoteSourceMock()

        let di = AppServices(testing: true, empty: true)
            .register(IAnswerLocalSource.self) { _ in local }
            .register(IAnswerRemoteSource.self) { _ in remote }
        AppServices.shared = di

        return (AnswerRepo(), local, remote)
    }

    func testAppServices() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IAnswerRepo.self))
    }

    func testGetAll() {
        let r = arrange()
        let answers = [Answer(id: 1, description: "1"), Answer(id: 2, description: "2")]
        r.localSource.stub.getAll = returnSuccess { answers }
        waitForSuccessfulCompletion(of: r.repo.getAll())
        XCTAssertEqual(r.localSource.verify.getAll.count, 1)
    }

    func testGet() {
        let r = arrange()
        let answer = Answer(id: 2, description: "2")
        r.localSource.stub.get = returnSuccess { answer }
        waitForSuccessfulCompletion(of: r.repo.get(id: 2))
        XCTAssertEqual(r.localSource.verify.get, [2])
    }

    func testPost() {
        let r = arrange()
        let answer = Answer(id: 1, description: "description")

        r.localSource.stub.createOrUpdate = returnSuccess {}
        r.remoteSource.stub.post = returnSuccess { answer }

        waitForSuccessfulCompletion(of: r.repo.post(answer: answer))
    }
    
    func testPostFailure() {
        let r = arrange()
        let answer = Answer(id: 1, description: "description")

        r.localSource.stub.createOrUpdate = returnSuccess {}
        r.remoteSource.stub.post = returnError { RestClientErrors.requestFailed(code: 400) }

        waitForFailureCompletion(of: r.repo.post(answer: answer))
    }
}
