//
//  AnswerRepoMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class AnswerRepoMock: IAnswerRepo {
    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var get: ((Int) -> AnyPublisher<Answer?, Never>)?
        @Unwrapable var getAll: (() -> AnyPublisher<[Answer], Never>)?
        @Unwrapable var post: ((Answer) -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var get: [Int] = []
        var getAll: [()] = []
        var post: [Answer] = []
    }

    func post(answer: Answer) -> AnyPublisher<Void, Error> {
        verify.post.append(answer)
        return stub.$post.safeValue()(answer)
    }

    func get(id: Int) -> AnyPublisher<Answer?, Never> {
        verify.get.append(id)
        return stub.$get.safeValue()(id)
    }

    func getAll() -> AnyPublisher<[Answer], Never> {
        verify.getAll.append(())
        return stub.$getAll.safeValue()()
    }
}
