//
//  QuestionsRepoMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class QuestionRepoMock: IQuestionRepo {
    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var get: ((Int) -> AnyPublisher<Question?, Never>)?
        @Unwrapable var getAll: (() -> AnyPublisher<[Question], Never>)?
        @Unwrapable var sync: (() -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var get: [Int] = []
        var getAll: [()] = []
        var sync: [()] = []
    }

    func sync() -> AnyPublisher<Void, Error> {
        verify.sync.append(())
        return stub.$sync.safeValue()()
    }

    func get(id: Int) -> AnyPublisher<Question?, Never> {
        verify.get.append(id)
        return stub.$get.safeValue()(id)
    }

    func getAll() -> AnyPublisher<[Question], Never> {
        verify.getAll.append(())
        return stub.$getAll.safeValue()()
    }
}
