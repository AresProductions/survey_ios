//
//  GetQuestionsWithAnswersUCTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 6/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetQuestionsWithAnswersUCTests: BaseTestCase {
    private func arrange() -> (uc: GetQuestionsWithAnswersUC,
                               getAllQuestionsUC: GetAllQuestionsUCMock,
                               getAllAnswersUC: GetAllAnswersUCMock)
    {
        let getAllQuestionsUC = GetAllQuestionsUCMock()
        let getAllAnswersUC = GetAllAnswersUCMock()
        let di = AppServices(testing: true, empty: true)
            .register(IGetAllQuestionsUC.self) { _ in getAllQuestionsUC }
            .register(IGetAllAnswersUC.self) { _ in getAllAnswersUC }
        AppServices.shared = di
        return (GetQuestionsWithAnswersUC(), getAllQuestionsUC, getAllAnswersUC)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IGetQuestionsWithAnswersUC.self))
    }

    func testExecute() {
        // given
        let r = arrange()
        let exampleQuestions = [Question(id: 1, description: "a nice question"),
                                Question(id: 2, description: "a nice question 2"),
                                Question(id: 3, description: "a nice question 3")]

        let exampleAnswer = [Answer(id: 2, description: "a nice answer 2")]

        let exampleQuestionsWithAnswers = [QuestionWithAnswer(id: 1, question: "a nice question", answer: nil),
                                           QuestionWithAnswer(id: 2, question: "a nice question 2", answer: "a nice answer 2"),
                                           QuestionWithAnswer(id: 3, question: "a nice question 3", answer: nil)]

        // when
        r.getAllQuestionsUC.stub.execute = returnSuccess { exampleQuestions }
        r.getAllAnswersUC.stub.execute = returnSuccess { exampleAnswer }

        // then
        waitForValue(of: r.uc.execute(), value: exampleQuestionsWithAnswers)
        XCTAssertEqual(r.getAllQuestionsUC.verify.execute.count, 1)
        XCTAssertEqual(r.getAllAnswersUC.verify.execute.count, 1)
    }
}
