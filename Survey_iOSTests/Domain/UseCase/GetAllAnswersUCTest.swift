//
//  GetAllAnswersUCTest.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 6/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetAllAnswersUCTests: BaseTestCase {
    private func arrange() -> (uc: GetAllAnswersUC, repo: AnswerRepoMock) {
        let repo = AnswerRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IAnswerRepo.self) { _ in repo }
        AppServices.shared = di
        return (GetAllAnswersUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IGetAllAnswersUC.self))
    }

    func testExecute() {
        let r = arrange()
        let exampleAnswers = [Answer(id: 1, description: "a nice question"),
                              Answer(id: 2, description: "a nice question 2"),
                              Answer(id: 3, description: "a nice question 3")]
        r.repo.stub.getAll = returnSuccess { exampleAnswers }
        waitForValue(of: r.uc.execute(), value: exampleAnswers)
        XCTAssertEqual(r.repo.verify.getAll.count, 1)
    }
}
