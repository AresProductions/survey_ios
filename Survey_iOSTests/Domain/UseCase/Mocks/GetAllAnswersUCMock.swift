//
//  GetAllAnswersUCMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 6/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetAllAnswersUCMock: IGetAllAnswersUC {
    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var execute: (() -> AnyPublisher<[Answer], Never>)?
    }

    class Verify {
        var execute: [()] = []
    }

    func execute() -> AnyPublisher<[Answer], Never> {
        verify.execute.append(())
        return stub.$execute.safeValue()()
    }
}

