//
//  GetQuestionsWithAnswersUCMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 8/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetQuestionsWithAnswersUCMock: IGetQuestionsWithAnswersUC {
    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var execute: (() -> AnyPublisher<[QuestionWithAnswer], Never>)?
    }

    class Verify {
        var execute: [()] = []
    }

    func execute() -> AnyPublisher<[QuestionWithAnswer], Never> {
        verify.execute.append(())
        return stub.$execute.safeValue()()
    }
}
