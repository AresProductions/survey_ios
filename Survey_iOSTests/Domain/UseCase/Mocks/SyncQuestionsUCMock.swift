//
//  SyncQuestionsUCMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class SyncQuestionsUCMock: ISyncQuestionsUC {

    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var execute: (() -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var execute: [()] = []
    }

    func execute() -> AnyPublisher<Void, Error> {
        verify.execute.append(())
        return stub.$execute.safeValue()()
    }
}

