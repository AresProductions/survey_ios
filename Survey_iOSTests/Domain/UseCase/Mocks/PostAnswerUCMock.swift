//
//  PostAnswerUCMock.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 8/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class PostAnswerUCMock: IPostAnswerUC {
    public let stub = Stub()
    public let verify = Verify()

    class Stub {
        @Unwrapable var execute: ((Answer) -> AnyPublisher<Void, Error>)?
    }

    class Verify {
        var execute: [Answer] = []
    }

    func execute(answer: Answer) -> AnyPublisher<Void, Error> {
        verify.execute.append(answer)
        return stub.$execute.safeValue()(answer)
    }
}
