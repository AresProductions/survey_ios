//
//  PostAnswerUCTest.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class PostAnswerUCTests: BaseTestCase {
    private func arrange() -> (uc: PostAnswerUC, repo: AnswerRepoMock) {
        let repo = AnswerRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IAnswerRepo.self) { _ in repo }
        AppServices.shared = di
        return (PostAnswerUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IPostAnswerUC.self))
    }

    func testExecute() {
        let r = arrange()
        let exampleAnswer = Answer(id: 2, description: "a nice answer")
        r.repo.stub.post = returnSuccess {}
        waitForSuccessfulCompletion(of: r.uc.execute(answer: exampleAnswer))
        XCTAssertEqual(r.repo.verify.post.count, 1)
    }

    func testExecuteError() {
        let r = arrange()
        let exampleAnswer = Answer(id: 2, description: "a nice answer")
        r.repo.stub.post = returnError { RestClientErrors.requestFailed(code: 400) }
        waitForFailureCompletion(of: r.uc.execute(answer: exampleAnswer))
        XCTAssertEqual(r.repo.verify.post.count, 1)
    }
}
