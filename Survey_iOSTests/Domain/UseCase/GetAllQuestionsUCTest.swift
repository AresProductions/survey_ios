//
//  GetAllQuestionsUCTest.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 6/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetAllQuestionsUCTests: BaseTestCase {
    private func arrange() -> (uc: GetAllQuestionsUC, repo: QuestionRepoMock) {
        let repo = QuestionRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IQuestionRepo.self) { _ in repo }
        AppServices.shared = di
        return (GetAllQuestionsUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IGetAllQuestionsUC.self))
    }

    func testExecute() {
        let r = arrange()
        let exampleQuestions = [Question(id: 1, description: "a nice question"),
                              Question(id: 2, description: "a nice question 2"),
                              Question(id: 3, description: "a nice question 3")]
        r.repo.stub.getAll = returnSuccess { exampleQuestions }
        waitForValue(of: r.uc.execute(), value: exampleQuestions)
        XCTAssertEqual(r.repo.verify.getAll.count, 1)
    }
}

