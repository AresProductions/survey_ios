//
//  GetQuestionUCTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetQuestionUCTests: BaseTestCase {
    private func arrange() -> (uc: GetQuestionUC, repo: QuestionRepoMock) {
        let repo = QuestionRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IQuestionRepo.self) { _ in repo }
        AppServices.shared = di
        return (GetQuestionUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IGetQuestionUC.self))
    }

    func testExecute() {
        let r = arrange()
        let exampleQuestion = Question(id: 1, description: "a nice question")
        r.repo.stub.get = returnSuccess { exampleQuestion }
        waitForValue(of: r.uc.execute(id: 1), value: exampleQuestion)
        XCTAssertEqual(r.repo.verify.get.count, 1)
    }
}
