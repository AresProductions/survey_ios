//
//  SyncQuestionsUCTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class SyncQuestionsUCTests: BaseTestCase {
    private func arrange() -> (uc: SyncQuestionsUC, repo: QuestionRepoMock) {
        let repo = QuestionRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IQuestionRepo.self) { _ in repo }
        AppServices.shared = di
        return (SyncQuestionsUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(ISyncQuestionsUC.self))
    }

    func testExecute() {
        let r = arrange()
        r.repo.stub.sync = returnSuccess {}
        waitForSuccessfulCompletion(of: r.uc.execute())
        XCTAssertEqual(r.repo.verify.sync.count, 1)
    }

    func testExecuteError() {
        let r = arrange()
        r.repo.stub.sync = returnError { RestClientErrors.requestFailed(code: 400) }
        waitForFailureCompletion(of: r.uc.execute())
        XCTAssertEqual(r.repo.verify.sync.count, 1)
    }
}
