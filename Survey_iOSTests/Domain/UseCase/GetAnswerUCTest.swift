//
//  GetAnswerUCTest.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class GetAnswerUCTests: BaseTestCase {
    private func arrange() -> (uc: GetAnswerUC, repo: AnswerRepoMock) {
        let repo = AnswerRepoMock()
        let di = AppServices(testing: true, empty: true)
            .register(IAnswerRepo.self) { _ in repo }
        AppServices.shared = di
        return (GetAnswerUC(), repo)
    }

    func testDI() {
        XCTAssertNotNil(AppServices(testing: true).tryResolve(IGetAnswerUC.self))
    }

    func testExecute() {
        let r = arrange()
        let exampleAnswer = Answer(id: 1, description: "a nice question")
        r.repo.stub.get = returnSuccess { exampleAnswer }
        waitForValue(of: r.uc.execute(id: 1), value: exampleAnswer)
        XCTAssertEqual(r.repo.verify.get.count, 1)
    }
}
