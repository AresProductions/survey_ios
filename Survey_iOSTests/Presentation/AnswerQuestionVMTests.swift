//
//  AnswerQuestionVMTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 8/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class AnswerQuestionVMTests: BaseTestCase {
    private func arrange(syncFail: Bool = false) -> (vm: AnswerQuestionVM,
                                                     getQuestionsWithAnswers: GetQuestionsWithAnswersUCMock,
                                                     getAllAnswers: GetAllAnswersUCMock,
                                                     postAnswer: PostAnswerUCMock)
    {
        let getQuestionsWithAnswers = GetQuestionsWithAnswersUCMock()
        let getAllAnswers = GetAllAnswersUCMock()
        let postAnswer = PostAnswerUCMock()

        let di = AppServices(testing: true, empty: true)
            .register(IGetQuestionsWithAnswersUC.self, instance: getQuestionsWithAnswers)
            .register(IGetAllAnswersUC.self, instance: getAllAnswers)
            .register(IPostAnswerUC.self, instance: postAnswer)

        AppServices.shared = di
        let questionsAndAnswersInit = questionsAndAnswers
        
        let answers = [Answer(id: 2, description: "answer 2"),
                       Answer(id: 3, description: "answer 3")]
        
        getQuestionsWithAnswers.stub.execute = returnSuccess { questionsAndAnswersInit }
        getAllAnswers.stub.execute = returnSuccess { answers }
        return (AnswerQuestionVM(), getQuestionsWithAnswers, getAllAnswers, postAnswer)
    }
    
    private let questionsAndAnswers = [QuestionWithAnswer(id: 0, question: "question", answer: nil),
                                       QuestionWithAnswer(id: 1, question: "question 2", answer: "answer 2"),
                                       QuestionWithAnswer(id: 2, question: "question 3", answer: "answer 3"),
                                       QuestionWithAnswer(id: 3, question: "question 4", answer: nil)]

    override func tearDown() {
        checkForMemoryLeak(AnswerQuestionVMSpy.self)
        super.tearDown()
    }

    func testGetQuestionsAndAnswers() {
        // given - when
        let r = arrange()

        // then
        waitForValue(of: r.vm.$questionsCount, value: 4)
        waitForValue(of: r.vm.$currentIndex, value: 0)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[0])
    }
    
    func testCurrentIndexFirst() {
        // given
        let r = arrange()

        // when
        r.vm.currentIndex = 0
        
        // then
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: true)
        waitForValue(of: r.vm.$nextButtonDisabled, value: false)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[0])
        
        // when
        r.vm.previousPage()
        
        // then
        waitForValue(of: r.vm.$currentIndex, value: 0)
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: true)
        waitForValue(of: r.vm.$nextButtonDisabled, value: false)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[0])
    }
    
    func testCurrentIndexLast() {
        // given
        let r = arrange()

        // when
        r.vm.currentIndex = 3
        
        // then
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: false)
        waitForValue(of: r.vm.$nextButtonDisabled, value: true)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[3])
        
        // when
        r.vm.nextPage()
        
        // then
        waitForValue(of: r.vm.$currentIndex, value: 3)
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: false)
        waitForValue(of: r.vm.$nextButtonDisabled, value: true)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[3])
    }
    
    func testCurrentIndexMiddle() {
        // given
        let r = arrange()

        // when
        r.vm.currentIndex = 2
        
        // then
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: false)
        waitForValue(of: r.vm.$nextButtonDisabled, value: false)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[2])
        
        // when
        r.vm.nextPage()
        
        // then
        waitForValue(of: r.vm.$currentIndex, value: 3)
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: false)
        waitForValue(of: r.vm.$nextButtonDisabled, value: true)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[3])
        
        // when
        r.vm.previousPage()
        
        // then
        waitForValue(of: r.vm.$currentIndex, value: 2)
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        waitForValue(of: r.vm.$previousButtonDisabled, value: false)
        waitForValue(of: r.vm.$nextButtonDisabled, value: false)
        waitForValue(of: r.vm.$currentQnA, value: questionsAndAnswers[2])
    }
    
    func testTextField() {
        // given - when
        let r = arrange()
        
        // then
        waitForValue(of: r.vm.$textFieldAnswer, value: "")
        waitForValue(of: r.vm.$submitButtonDisabled, value: true)
        
        // when
        r.vm.textFieldAnswer = "answer"
        waitForValue(of: r.vm.$submitButtonDisabled, value: false)
    }
    
    func testPostAnswer() {
        // given
        let r = arrange()
        r.postAnswer.stub.execute = returnSuccess { () }
        r.vm.currentQnA = questionsAndAnswers[3]
        r.vm.textFieldAnswer = "answer 4"
        
        // when
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        r.vm.postAnswer()
        
        // then
        waitForValue(of: r.vm.$postAnswerState, value: .inProgress)
        waitForSuccessfulCompletion(of: r.postAnswer.execute(answer: Answer(id: r.vm.currentIndex,
                                                                            description: r.vm.textFieldAnswer)))

        waitForValue(of: r.vm.$postAnswerState, value: .success)
        waitForValue(of: r.vm.$postViewStateGone, value: true)
        waitForValue(of: r.vm.$postAnswerStateString, value: "Success!")
    }
    
    func testPostAnswerFail() {
        // given
        let r = arrange()
        r.postAnswer.stub.execute = returnError { RestClientErrors.requestFailed(code: 400) }
        r.vm.currentQnA = questionsAndAnswers[3]
        r.vm.textFieldAnswer = "answer 4"
        
        // when
        waitForValue(of: r.vm.$postAnswerState, value: .idle)
        r.vm.postAnswer()
        
        // then
        waitForValue(of: r.vm.$postAnswerState, value: .inProgress)
        waitForFailureCompletion(of: r.postAnswer.execute(answer: Answer(id: r.vm.currentIndex,
                                                                         description: r.vm.textFieldAnswer)))

        waitForValue(of: r.vm.$postAnswerState, value: .fail)
        waitForValue(of: r.vm.$postViewStateGone, value: false)
        waitForValue(of: r.vm.$postAnswerStateString, value: "Failed to submit your answer :(\nPlease try again...")
    }
}

private class AnswerQuestionVMSpy: AnswerQuestionVM, ReferenceCount {
    static var referenceCount: Int = 0

    override init() {
        AnswerQuestionVMSpy.referenceCount += 1
        super.init()
    }

    deinit {
        AnswerQuestionVMSpy.referenceCount -= 1
    }
}
