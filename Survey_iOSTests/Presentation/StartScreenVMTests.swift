//
//  StartScreenVMTests.swift
//  Survey_iOSTests
//
//  Created by Ares Ceka on 7/12/20.
//

import Combine
@testable import Survey_iOS
import XCTest

class StartScreenVMTests: BaseTestCase {
    private func arrange(syncFail: Bool = false) -> (vm: StartSurveyVM, syncQuestions: SyncQuestionsUCMock) {
        let syncQuestions = SyncQuestionsUCMock()
        
        let di = AppServices(testing: true, empty: true)
            .register(ISyncQuestionsUC.self, instance: syncQuestions)
        AppServices.shared = di
        
        if syncFail {
            syncQuestions.stub.execute = returnError { RestClientErrors.noDataReceived }
        } else {
            syncQuestions.stub.execute = returnSuccess {()}
        }
        return (StartSurveyVM(), syncQuestions)
    }
    
    override func tearDown() {
        checkForMemoryLeak(StartSurveyVMSpy.self)
        super.tearDown()
    }
    
    func testSyncOnInit() {
        // given
        let r = arrange()
        waitForValue(of: r.vm.$requestQuestionsState, value: .inProgress)
        waitForValue(of: r.vm.$buttonDisabled, value: true)
        waitForValue(of: r.vm.$stateString, value: "Downloading...")

        // then
        waitForSuccessfulCompletion(of: r.syncQuestions.execute())
        waitForValue(of: r.vm.$buttonDisabled, value: false)
        waitForValue(of: r.vm.$stateString, value: "Ready!")
        waitForValue(of: r.vm.$requestQuestionsState, value: .success)
    }
    
    func testSyncOnInitFail() {
        // given
        let r = arrange(syncFail: true)
        waitForValue(of: r.vm.$requestQuestionsState, value: .inProgress)
        waitForValue(of: r.vm.$buttonDisabled, value: true)
        
        // then
        waitForValue(of: r.vm.$buttonDisabled, value: true)
        waitForValue(of: r.vm.$stateString, value: "Failed to download questions :(")
        waitForValue(of: r.vm.$requestQuestionsState, value: .fail)
        waitForFailureCompletion(of: r.syncQuestions.execute())

    }
}


private class StartSurveyVMSpy: StartSurveyVM, ReferenceCount {
    static var referenceCount: Int = 0

    override init() {
        StartSurveyVMSpy.referenceCount += 1
        super.init()
    }

    deinit {
        StartSurveyVMSpy.referenceCount -= 1
    }
}
