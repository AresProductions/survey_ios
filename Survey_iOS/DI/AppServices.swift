//
//  AppServices.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import CoreData
import Foundation
import UIKit

/// Provides the Survey_iOS necessary services.
class AppServices: DIContainer {
    /// Whether we are in testing mode.
    private let testing: Bool

    /// Prepares a new `DIContainer` with all app-relevant services.
    init(testing: Bool = false, empty: Bool = false) {
        self.testing = testing

        super.init()
        DIContainer.default = self

        if !empty {
            registerSingletons()
            registerRemoteSources()
            registerLocalSources()
            registerRepositories()
            registerUseCases()
        }
    }

    /// A global default singleton instance.
    static var shared = AppServices(testing: false, empty: true)

    /// Registers singleton instances.
    private func registerSingletons() {
        let persistentContainer = NSPersistentContainer.createForSurveys(testing: testing)
        let restClient = RestClient()
        register(IRestClient.self, instance: restClient)
        register(instance: persistentContainer)
    }

    /// Register remote data sources.
    private func registerRemoteSources() {
        register(IQuestionRemoteSource.self) { _ in QuestionRemoteSource() }
        register(IAnswerRemoteSource.self) { _ in AnswerRemoteSource() }
    }

    /// Register local data sources.
    private func registerLocalSources() {
        register(IQuestionLocalSource.self) { _ in QuestionLocalSource() }
        register(IAnswerLocalSource.self) { _ in AnswerLocalSource() }
    }

    /// Register repositories.
    private func registerRepositories() {
        register(IQuestionRepo.self) { _ in QuestionRepo() }
        register(IAnswerRepo.self) { _ in AnswerRepo() }
    }

    /// Registers use cases.
    private func registerUseCases() {
        register(ISyncQuestionsUC.self) { _ in SyncQuestionsUC() }
        register(IGetQuestionUC.self) { _ in GetQuestionUC() }
        register(IGetAnswerUC.self) { _ in GetAnswerUC() }
        register(IGetAllQuestionsUC.self) { _ in GetAllQuestionsUC() }
        register(IGetAllAnswersUC.self) { _ in GetAllAnswersUC() }
        register(IGetQuestionsWithAnswersUC.self) { _ in GetQuestionsWithAnswersUC() }
        register(IPostAnswerUC.self) { _ in PostAnswerUC() }
    }
}
