//
//  DependencyInjection.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import SwiftUI

/*
 * COPY - PASTED
 */

// MARK: - Interfaces

/// Provides means to get an instance of a certain type.
protocol IDIResolver {
    /// Resolves an instance of type `T`.
    ///
    /// - Throws: An error is thrown if there is no instance of type `T`.
    func resolve<T>(_ type: T.Type?) -> T

    /// Resolves an instance of type `T`.
    ///
    /// - Returns: Either an instance of type `T` or `nil`.
    func tryResolve<T>(_ type: T.Type?) -> T?
}

/// A factory that knows how to create instances of type `InstanceType`
protocol IDIFactory {
    /// The type of the instances this factory can create.
    associatedtype InstanceType

    /// Creates a new instance.
    func make(resolver: IDIResolver) -> InstanceType
}

// MARK: - Implementation

/// Provides `nil`-default versions for the `type` parameter.
extension IDIResolver {
    func resolve<T>(_ type: T.Type? = nil) -> T {
        resolve(type)
    }

    func tryResolve<T>(_ type: T.Type? = nil) -> T? {
        tryResolve(type)
    }
}

/// A container for factories which are used to resolve instances for types.
class DIContainer: IDIResolver {
    private var factories = [ObjectIdentifier: DIAnyFactory]()

    /// A global default singleton instance.
    ///
    /// This instance is used by the `Injected` and `InjectedObservedObject`
    /// property wrappers.
    static var `default`: DIContainer = DIContainer()

    // MARK: Registering instances and factories

    /// Registers a singleton instance.
    ///
    /// ```
    /// container.register(MyConcreteType() as IMyInterface)
    /// ```
    @discardableResult func register<T>(instance: T) -> Self {
        return register(T.self) { _ in instance }
    }

    /// Registers a singleton instance.
    ///
    /// ```
    /// container.register(IMyInterface.self, instance: MyConcreteType())
    /// ```
    @discardableResult func register<T>(_ type: T.Type, instance: T) -> Self {
        return register(type) { _ in instance }
    }

    /// Registers a factory.
    ///
    /// ```
    /// container.register { MyConcreteType() as IMyInterface }
    /// ```
    @discardableResult func register<T>(_ factory: @escaping (IDIResolver) -> T) -> Self {
        return register(T.self, factory)
    }

    /// Registers a factory.
    ///
    /// ```
    /// container.register(IMyInterface.self) { MyConcreteType() }
    /// ```
    @discardableResult func register<T>(_ type: T.Type, _ factory: @escaping (IDIResolver) -> T) -> Self {
        guard getFactory(for: type) == nil else {
            fatalError("DI: factory already registered for type \(type)")
        }

        factories[ObjectIdentifier(type)] = DIAnyFactory(for: type, factory)

        return self
    }

    // MARK: IDIResolver implementation

    func resolve<T>(_ type: T.Type? = nil) -> T {
        guard let t: T = tryResolve() else {
            fatalError("DI: no factory available for the requested type \(T.self)")
        }

        return t
    }

    func tryResolve<T>(_ type: T.Type? = nil) -> T? {
        // if the requested type is an optional, we try to get the wrapped
        // type and not the optional type...
        if let optional = T.self as? OptionalProtocol.Type {
            let t = optional.wrappedType()

            guard let factory = getFactory(for: t) else {
                return nil
            }

            return Optional.some(factory.make(resolver: self)) as? T
        }

        return getFactory(for: T.self)?.make(resolver: self) as? T
    }

    // MARK: Private helper methods

    private func getFactory(for type: Any.Type) -> DIAnyFactory? {
        factories[ObjectIdentifier(type)]
    }
}

/// Provides means to statically get the wrapped type.
protocol OptionalProtocol {
    static func wrappedType() -> Any.Type
}

extension Optional: OptionalProtocol {
    static func wrappedType() -> Any.Type {
        return Wrapped.self
    }
}

/// Generic implementation of `DIFactory`.
///
/// Since this class drops all the type checking, it has to be
/// ensured during instantiation that everything is correct.
private class DIAnyFactory: IDIFactory {
    typealias InstanceType = Any

    private let factory: (IDIResolver) -> Any

    init<T>(for type: T.Type, _ factory: @escaping (IDIResolver) -> T) {
        self.factory = factory
    }

    func make(resolver: IDIResolver) -> Any {
        factory(resolver)
    }
}

/// Resolves the injected type using the global `DIContainer.default` resolver.
@propertyWrapper
struct Injected<T> {
    private var value: T

    init() {
        value = DIContainer.default.resolve()
    }

    public var wrappedValue: T {
        get { value }
        mutating set { value = newValue }
    }
}

/// Resolves the injected type using the global `DIContainer.default` resolver.
@propertyWrapper
struct InjectedObservedObject<T: ObservableObject>: DynamicProperty {
    @ObservedObject private var value: T

    init() {
        value = DIContainer.default.resolve()
    }

    public var wrappedValue: T {
        get { value }
        mutating set { value = newValue }
    }

    public var projectedValue: ObservedObject<T>.Wrapper {
        _value.projectedValue
    }
}
