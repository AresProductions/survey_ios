//
//  File.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
import Foundation

class AnswerRepo: IAnswerRepo {
    @Injected private var answerLocalSource: IAnswerLocalSource
    @Injected private var answerRemoteSource: IAnswerRemoteSource

    func post(answer: Answer) -> AnyPublisher<Void, Error> {
        return answerRemoteSource.post(answer: answer)
            .map { [weak self] answer in
                _ = self?.answerLocalSource.createOrUpdate(answer: answer)
            }.eraseToAnyPublisher()
    }

    func get(id: Int) -> AnyPublisher<Answer?, Never> {
        return answerLocalSource.get(id: id)
    }
    
    func getAll() -> AnyPublisher<[Answer], Never> {
        return answerLocalSource.getAll()
    }
}
