//
//  QuestionsRepoImpl.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 2/12/20.
//

import Combine
import Foundation

class QuestionRepo: IQuestionRepo {
    @Injected private var questionRemoteSource: IQuestionRemoteSource
    @Injected private var questionLocalSource: IQuestionLocalSource
    func sync() -> AnyPublisher<Void, Error> {
        questionRemoteSource.getAll()
            .map { [weak self] questions in
                _ = self?.questionLocalSource.createOrUpdate(questions: questions)
            }.eraseToAnyPublisher()
    }

    func get(id: Int) -> AnyPublisher<Question?, Never> {
        return questionLocalSource.get(id: id)
    }
    
    func getAll() -> AnyPublisher<[Question], Never> {
        questionLocalSource.getAll()
    }
}
