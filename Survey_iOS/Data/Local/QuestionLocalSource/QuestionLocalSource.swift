//
//  QuestionLocalSource.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import CoreData
import Foundation

protocol IQuestionLocalSource {
    /// Get all `DQuestion`s
    func getAll() -> AnyPublisher<[Question], Never>

    /// Get `DQuestion` based on `id`
    func get(id: Int) -> AnyPublisher<Question?, Never>

    /// Updates or creates the given `DQuestion`s in the database.
    func createOrUpdate(questions: [Question]) -> AnyPublisher<Void, Error>
}

class QuestionLocalSource: IQuestionLocalSource {
    @Injected private var container: NSPersistentContainer

    func getAll() -> AnyPublisher<[Question], Never> {
        let fetchRequest: NSFetchRequest<DQuestion> = DQuestion.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        return FetchPublisher(request: fetchRequest, context: container.viewContext)
            .map { dQuestions in
                dQuestions.map { $0.mapToDomain() }
            }
            .eraseToAnyPublisher()
    }
    
    func get(id: Int) -> AnyPublisher<Question?, Never> {
        let fetchRequest: NSFetchRequest<DQuestion> = DQuestion.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %d", id) //NSPredicate(format: "%K = %@", #keyPath(DQuestion.id), id) Xcode 12 bug?
        return FetchPublisher(request: fetchRequest,
                              context: container.viewContext)
            .map { $0.first?.mapToDomain() }
            .eraseToAnyPublisher()
    }

    func createOrUpdate(questions: [Question]) -> AnyPublisher<Void, Error> {
        container.update { context in
            questions.forEach { question in
                let fetchRequest = DQuestion.fetchRequest(for: question.id)
                let dQuestion = (try? context.fetchOne(using: fetchRequest)) ?? DQuestion(context: context)
                dQuestion.updateValues(using: question)
            }
            try context.saveIfChanged()
        }.eraseToAnyPublisher()
    }
}
