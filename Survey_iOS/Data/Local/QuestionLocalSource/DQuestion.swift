//
//  DQuestion.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import CoreData
import Foundation

extension DQuestion {
    func mapToDomain() -> Question {
        Question(id: Int(id), description: question ?? "")
    }
}

extension DQuestion {
    static func fetchRequest(for id: Int) -> NSFetchRequest<DQuestion> {
        let request: NSFetchRequest<DQuestion> = DQuestion.fetchRequest()
        request.predicate = NSPredicate(format: "id = %d", id)
        return request
    }

    static func get(for id: Int,
                    context: NSManagedObjectContext) throws -> DQuestion?
    {
        return try context.fetch(DQuestion.fetchRequest(for: id)).first
    }
}

extension DQuestion: SynchronizableManagedObject {
    typealias OtherObject = Question

    func updateValues(using object: Question) {
        self.id = Int16(object.id)
        self.question = object.description
    }
}
