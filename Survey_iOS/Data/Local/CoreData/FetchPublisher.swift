//
//  FetchPublisher.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import CoreData
import Foundation

/*
 * COPY PASTED CODE https://medium.com/better-programming/combine-publishers-and-core-data-424b68fe9473
 */

/// Publisher for a fetchrequest for the given context
class FetchPublisher<Entity>: NSObject, NSFetchedResultsControllerDelegate, Publisher
    where Entity: NSManagedObject {
    typealias Output = [Entity]
    typealias Failure = Never

    private let request: NSFetchRequest<Entity>
    private let context: NSManagedObjectContext
    private let subject: CurrentValueSubject<Output, Failure>
    private var resultController: NSFetchedResultsController<NSManagedObject>?
    private var subscriptions = 0

    init(request: NSFetchRequest<Entity>, context: NSManagedObjectContext) {
        if request.sortDescriptors == nil { request.sortDescriptors = [] }
        self.request = request
        self.context = context
        subject = CurrentValueSubject([])
        super.init()
    }

    // MARK: Publisher implementation

    func receive<S>(subscriber: S)
        where S: Subscriber, FetchPublisher.Failure == S.Failure, FetchPublisher.Output == S.Input {
        var start = false

        objc_sync_enter(self)
        subscriptions += 1
        start = subscriptions == 1
        objc_sync_exit(self)

        if start {
            let controller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context,
                                                        sectionNameKeyPath: nil, cacheName: nil)
            controller.delegate = self

            do {
                try controller.performFetch()
                let result = controller.fetchedObjects ?? []
                subject.send(result)
            } catch {
                _ = print("Failed to perform fetch: \(String(describing: error))")
            }
            resultController = controller as? NSFetchedResultsController<NSManagedObject>
        }
        FetchPublisherSubscription(fetchPublisher: self, subscriber: AnySubscriber(subscriber))
    }

    // MARK: NSFetchedResultsControllerDelegate implementation

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        let result = controller.fetchedObjects as? [Entity] ?? []
        subject.send(result)
    }

    // MARK: Private helper stuff

    private func dropSubscription() {
        objc_sync_enter(self)
        subscriptions -= 1
        let stop = subscriptions == 0
        objc_sync_exit(self)

        if stop {
            resultController?.delegate = nil
            resultController = nil
        }
    }

    private class FetchPublisherSubscription: Subscription {
        private var fetchPublisher: FetchPublisher?
        private var cancellable: AnyCancellable?

        @discardableResult
        init(fetchPublisher: FetchPublisher, subscriber: AnySubscriber<Output, Failure>) {
            self.fetchPublisher = fetchPublisher

            subscriber.receive(subscription: self)

            cancellable = fetchPublisher.subject.sink(receiveCompletion: { completion in
                subscriber.receive(completion: completion)
            }, receiveValue: { value in
                _ = subscriber.receive(value)
            })
        }

        func request(_ demand: Subscribers.Demand) {}

        func cancel() {
            cancellable?.cancel()
            cancellable = nil
            fetchPublisher?.dropSubscription()
            fetchPublisher = nil
        }
    }
}
