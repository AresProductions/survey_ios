//
//  NSPersistentContainer+Extensions.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import CoreData
import Foundation

/*
 * Copy Pasted Code
 */

enum CoreDataError: Error {
    /// Although we expected at most 1 object, we got more than that.
    case extraObjects
}

private var model = NSManagedObjectModel.mergedModel(from: [Bundle.main])!

// MARK: - Interfaces

/// Protocol for automatically updating arbitary managed objects using other objects
protocol SynchronizableManagedObject: NSManagedObject {
    /// The type of the object we want to use for updating the `NSManagedObject`
    associatedtype OtherObject

    /// Updates the `NSManagedObject` using the `OtherObject`
    func updateValues(using object: OtherObject)
}

// MARK: - Extensions

extension NSPersistentContainer {
    // MARK: Creation and Deletion

    /// The location of the sqlite store for this container
    /// The location is based on the name, thus, in testing, you should use a random name for the container.
    private var sqliteLocation: URL {
        let directory = NSPersistentContainer.defaultDirectoryURL()
        let filename = name + ".sqlite"
        return directory.appendingPathComponent(filename)
    }

    /// Creates the CoreData container as we need it
    static func createForSurveys(testing: Bool = false) -> NSPersistentContainer {
        let container =
            NSPersistentContainer(name: "survey" + (testing ? "-testing-\(UUID().uuidString)" : ""),
                                  managedObjectModel: model)

        let description = NSPersistentStoreDescription()
        description.type = NSSQLiteStoreType
        description.url = container.sqliteLocation

        container.persistentStoreDescriptions = [description]
        container.tryLoadStores()

        // get notified when we merge stuff
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.shouldDeleteInaccessibleFaults = true
        return container
    }

    /// Tries to load the peristent store, if it fails it wll recreate the DB
    private func tryLoadStores() {
        loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                print("Failed to load Persistent Store, will recreate DB ", error.localizedDescription)
                self.clearDatabase()
            }
        })
    }

    /// Drops the entire database and recreates it.
    func clearDatabase() {
        do {
            try persistentStoreCoordinator.destroyPersistentStore(at: sqliteLocation,
                                                                  ofType: NSSQLiteStoreType,
                                                                  options: nil)
        } catch {
            print("Failed to delete CoreData sqlite store: \(String(describing: error))")
        }

        loadStoresOrFail()
    }

    private func loadStoresOrFail() {
        loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible,
                 * due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }

    // MARK: - Modifying

    // we need a memory address
    private struct AssociatedKeys {
        static var updateContextKey: UInt8 = 0
    }

    /// We use **one** dedicated update context to perform updates serially (in the background)
    /// to not provoke merge errors and to ensure that success update calls have the data of the previous one.
    var updateContext: NSManagedObjectContext {
        var moc = objc_getAssociatedObject(self, &AssociatedKeys.updateContextKey) as? NSManagedObjectContext

        if moc == nil {
            moc = newBackgroundContext()
            moc!.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy

            objc_setAssociatedObject(self, &AssociatedKeys.updateContextKey, moc, .OBJC_ASSOCIATION_RETAIN)
        }

        return moc!
    }

    /// Creates a new background context and runs the closure to perform changes in.
    ///
    /// This is very useful for altering the data in the background, as the original
    /// viewContext is simply overwritten by the changes performed here.
    @discardableResult func update<T>(blocking: Bool = false,
                                      task: @escaping (NSManagedObjectContext) throws -> T)
        -> Future<T, Error> {
        return Future<T, Error> { promise in
            let context = self.updateContext
            let perform = blocking ? context.performAndWait : context.perform

            perform {
                // perform our work!
                do { promise(.success(try task(context))) } catch {
                    print("Background update task failed: ", String(describing: error))
                    return promise(.failure(error))
                }
            }
        }
    }

    // MARK: - Read-only operations

    /// Runs the given `action` synchronously on the thread-specific context.
    ///
    /// Make sure that `NSManagedObjects` do not out-live the `action` closure, as this might cause
    /// mis-use by different threads.
    func use<T>(action: (NSManagedObjectContext) throws -> T) throws -> T {
        let context: NSManagedObjectContext

        if Thread.isMainThread {
            context = viewContext
        } else {
            context = newBackgroundContext()
        }

        var result: T?
        var optError: Error?

        context.performAndWait {
            do {
                result = try action(context)
            } catch {
                optError = error
            }
        }

        if let error = optError {
            throw error
        }

        return result!
    }

    /// Gets all objects from the appropriate context and makes them available in `andUseIn action:`.
    @discardableResult
    func fetchAll<T: NSManagedObject, R>(using request: NSFetchRequest<T>,
                                         andUseIn action: @escaping ([T]) throws -> R) throws -> R {
        try use { context in
            try action(try context.fetchAll(using: request))
        }
    }

    /// Gets one object from the appropriate context and makes them available in `andUseIn action:`.
    @discardableResult
    func fetchOne<T: NSManagedObject, R>(using request: NSFetchRequest<T>,
                                         andUseIn action: @escaping (T?) throws -> R) throws -> R {
        try use { context in
            try action(try context.fetchOne(using: request))
        }
    }
}
