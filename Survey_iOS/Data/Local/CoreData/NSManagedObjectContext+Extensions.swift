//
//  NSManagedObjectContext+Extensions.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import CoreData
import Foundation

extension NSManagedObjectContext {
    /// Gets all objects from the context.
    ///
    /// All these methods must run a `performAndWait` or `perform` closure!
    func fetchAll<T: NSManagedObject>(using request: NSFetchRequest<T>) throws -> [T] {
        return try fetch(request)
    }

    /// If the `request` is targeting just one object (i.e., primary key lookup), use this method
    /// which throws an `Error` if there is more than one object.
    func fetchOne<T: NSManagedObject>(using request: NSFetchRequest<T>) throws -> T? {
        let objects = try fetchAll(using: request)
        switch objects.count {
        case 0: return nil
        case 1: return objects[0]
        case 2...: throw CoreDataError.extraObjects
        default: fatalError()
        }
    }

    /// Tries to `save()` the context if it `hasChanges`.
    ///
    /// - Returns: `true` if there were changes saved
    @discardableResult func saveIfChanged() throws -> Bool {
        if hasChanges {
            try save()
            return true
        }

        return false
    }
}
