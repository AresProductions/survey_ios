//
//  AnswerLocalSource.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 4/12/20.
//

import Combine
import CoreData

protocol IAnswerLocalSource {
    /// Get all `DAnswer`s
    func getAll() -> AnyPublisher<[Answer], Never>

    /// Get `DAnswer` based on `id`
    func get(id: Int) -> AnyPublisher<Answer?, Never>

    /// Updates or creates the given `DAnswer` in the database.
    func createOrUpdate(answer: Answer) -> AnyPublisher<Void, Error>
}

class AnswerLocalSource: IAnswerLocalSource {
    @Injected private var container: NSPersistentContainer

    func getAll() -> AnyPublisher<[Answer], Never> {
        let fetchRequest: NSFetchRequest<DAnswer> = DAnswer.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        return FetchPublisher(request: fetchRequest, context: container.viewContext)
            .map { dAnswers in
                dAnswers.map { $0.mapToDomain() }
            }
            .eraseToAnyPublisher()
    }
    
    func get(id: Int) -> AnyPublisher<Answer?, Never> {
        let fetchRequest: NSFetchRequest<DAnswer> = DAnswer.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %d", id) //NSPredicate(format: "%K = %@", #keyPath(DAnswer.id), id) Xcode 12 bug?
        return FetchPublisher(request: fetchRequest,
                              context: container.viewContext)
            .map { $0.first?.mapToDomain() }
            .eraseToAnyPublisher()
    }

    func createOrUpdate(answer: Answer) -> AnyPublisher<Void, Error> {
        container.update { context in
            let fetchRequest = DAnswer.fetchRequest(for: answer.id)
            let dAnswer = (try? context.fetchOne(using: fetchRequest)) ?? DAnswer(context: context)
            dAnswer.updateValues(using: answer)
            try context.saveIfChanged()
        }.eraseToAnyPublisher()
    }
}
