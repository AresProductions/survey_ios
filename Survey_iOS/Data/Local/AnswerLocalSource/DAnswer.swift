//
//  DAnswer.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import CoreData
import Foundation

extension DAnswer {
    func mapToDomain() -> Answer {
        Answer(id: Int(id), description: answer ?? "")
    }
}

extension DAnswer {
    static func fetchRequest(for id: Int) -> NSFetchRequest<DAnswer> {
        let request: NSFetchRequest<DAnswer> = DAnswer.fetchRequest()
        request.predicate = NSPredicate(format: "id = %d", id)
        return request
    }

    static func get(for id: Int,
                    context: NSManagedObjectContext) throws -> DAnswer?
    {
        return try context.fetch(DAnswer.fetchRequest(for: id)).first
    }
}

extension DAnswer: SynchronizableManagedObject {
    typealias OtherObject = Answer

    func updateValues(using object: Answer) {
        self.id = Int16(object.id)
        self.answer = object.description
    }
}
