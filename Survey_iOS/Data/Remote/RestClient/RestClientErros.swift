//
//  RestClientErros.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Foundation

enum RestClientErrors: Error {
    case requestFailed(error: Error)
    case requestFailed(code: Int)
    case noDataReceived
    case jsonDecode(error: Error)
}
