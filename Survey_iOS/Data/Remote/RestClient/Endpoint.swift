//
//  Endpoint.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Foundation

protocol Endpoint {
    var url: URL { get }
    var path: String { get }
}

/// BaseUrl of Survey API Endpoint
private let baseURL = URL(string: "https://powerful-peak-54206.herokuapp.com/")

/// Survey API Endpoints
enum SurveyApiEndpoint: Endpoint {

    /// Allows API users who do not use RapidApi account to know the status of their subscription
    case questions
    /// Get all available fixtures from one {date}
    case questionSubmit

    var path: String {
        switch self {
        case .questions: return "questions"
        case .questionSubmit: return "question/submit"
        }
    }

    var url: URL {
        return URL(string: self.path, relativeTo: baseURL)!
    }
}
