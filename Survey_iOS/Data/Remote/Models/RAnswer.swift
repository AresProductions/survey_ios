//
//  RAnswer.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Foundation

struct RAnswer: Codable {
    let id: Int
    let answer: String
}

extension RAnswer {
    func mapToDomain() -> Answer {
        Answer(id: id, description: answer)
    }
}
