//
//  RQuestion.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Foundation

struct RQuestion: Codable {
    let id: Int
    let question: String
}

extension RQuestion {
    func mapToDomain() -> Question {
        Question(id: id, description: question)
    }
}
