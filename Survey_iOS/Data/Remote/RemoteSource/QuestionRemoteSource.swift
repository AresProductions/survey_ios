//
//  Question.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
import Foundation

protocol IQuestionRemoteSource {
    func getAll() -> AnyPublisher<[Question], Error>
}

class QuestionRemoteSource: IQuestionRemoteSource {
    @Injected private var restClient: IRestClient

    func getAll() -> AnyPublisher<[Question], Error> {
        restClient.get(SurveyApiEndpoint.questions)
            .map { (rQuestions: [RQuestion]) in
                rQuestions.map { $0.mapToDomain() }
            }.eraseToAnyPublisher()
    }
}
