//
//  AnswerRemoteSource.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import Combine

protocol IAnswerRemoteSource {
    func post(answer: Answer) -> AnyPublisher<Answer, Error>
}

class AnswerRemoteSource: IAnswerRemoteSource {
    @Injected private var restClient: IRestClient

    func post(answer: Answer) -> AnyPublisher<Answer, Error> {
        restClient.post(SurveyApiEndpoint.questionSubmit, using: answer.mapToRemote())
            .map { _ in
                answer
            }.eraseToAnyPublisher()
    }
}
