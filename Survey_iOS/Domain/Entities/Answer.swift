//
//  Answer.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Foundation

struct Answer: Equatable {
    let id: Int
    let description: String
}

extension Answer {
    func mapToRemote() -> RAnswer {
        RAnswer(id: id, answer: description)
    }
}
