//
//  QuestionWithAnswer.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation

struct QuestionWithAnswer: Equatable {
    let id: Int
    let question: String
    let answer: String?
}
