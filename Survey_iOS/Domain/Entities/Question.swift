//
//  Question.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Foundation

struct Question: Equatable {
    let id: Int
    let description: String
}
