//
//  GetAnswersUC.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import Combine

/// Observe all `Answer`s
protocol IGetAllAnswersUC {
    func execute() -> AnyPublisher<[Answer], Never>
}

class GetAllAnswersUC: IGetAllAnswersUC {
    @Injected private var answerRepo: IAnswerRepo

    func execute() -> AnyPublisher<[Answer], Never> {
        return answerRepo.getAll()
    }
}
