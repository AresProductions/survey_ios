//
//  GetQuestionsUC.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import Combine

/// Observe all `Question`s
protocol IGetAllQuestionsUC {
    func execute() -> AnyPublisher<[Question], Never>
}

class GetAllQuestionsUC: IGetAllQuestionsUC {
    @Injected private var questionRepo: IQuestionRepo

    func execute() -> AnyPublisher<[Question], Never> {
        return questionRepo.getAll()
    }
}
