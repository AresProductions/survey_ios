//
//  GetQuestionUC.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

/// Observe `Question`based on `id`
protocol IGetQuestionUC {
    func execute(id: Int) -> AnyPublisher<Question?, Never>
}

class GetQuestionUC: IGetQuestionUC {
    @Injected private var questionRepo: IQuestionRepo

    func execute(id: Int) -> AnyPublisher<Question?, Never> {
        return questionRepo.get(id: id)
    }
}
