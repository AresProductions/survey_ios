//
//  GetAnswer.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

/// Observe `Answer`based on `id`
protocol IGetAnswerUC {
    func execute(id: Int) -> AnyPublisher<Answer?, Never>
}

class GetAnswerUC: IGetAnswerUC {
    @Injected private var answerRepo: IAnswerRepo

    func execute(id: Int) -> AnyPublisher<Answer?, Never> {
        return answerRepo.get(id: id)
    }
}
