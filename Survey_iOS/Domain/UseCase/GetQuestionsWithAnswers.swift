//
//  GetQuestionsWithAnswers.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import Combine

/// Created a array of `QuestionWithAnswer`by coupling together the `Question`s and `Answer`s by `id`
protocol IGetQuestionsWithAnswersUC {
    func execute() -> AnyPublisher<[QuestionWithAnswer], Never>
}

class GetQuestionsWithAnswersUC: IGetQuestionsWithAnswersUC {
    @Injected private var getQuestionsUC: IGetAllQuestionsUC
    @Injected private var getAnswersUC: IGetAllAnswersUC

    func execute() -> AnyPublisher<[QuestionWithAnswer], Never> {
        let answersPubl = getAnswersUC.execute()
        let questionsPubl = getQuestionsUC.execute().receive(on: DispatchQueue.main)
        
        return Publishers.CombineLatest(questionsPubl, answersPubl)
            .map { questions, answers -> [QuestionWithAnswer] in
                questions.map { question -> QuestionWithAnswer in
                    let answer = answers.first { $0.id == question.id }
                    return QuestionWithAnswer(id: question.id, question: question.description, answer: answer?.description)
                }
            }.eraseToAnyPublisher()
    }
}
