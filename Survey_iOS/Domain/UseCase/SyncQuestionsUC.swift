//
//  GetQuestionsUC.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

/// Sync `Question`s
protocol ISyncQuestionsUC {
    func execute() -> AnyPublisher<Void, Error>
}

class SyncQuestionsUC: ISyncQuestionsUC {
    @Injected private var questionRepo: IQuestionRepo

    func execute() -> AnyPublisher<Void, Error> {
        return questionRepo.sync()
    }
}
