//
//  SendAnwerUC.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

/// Send an `Answer`
protocol IPostAnswerUC {
    func execute(answer: Answer) -> AnyPublisher<Void, Error>
}

class PostAnswerUC: IPostAnswerUC {
    @Injected private var answerRepo: IAnswerRepo

    func execute(answer: Answer) -> AnyPublisher<Void, Error> {
        return answerRepo.post(answer: answer)
    }
}
