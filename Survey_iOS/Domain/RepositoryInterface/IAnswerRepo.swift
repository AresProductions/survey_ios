//
//  IAnswerRepo.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

protocol IAnswerRepo {
    func post(answer: Answer) -> AnyPublisher<Void, Error>

    func get(id: Int) -> AnyPublisher<Answer?, Never>
    
    func getAll() -> AnyPublisher<[Answer], Never>
}
