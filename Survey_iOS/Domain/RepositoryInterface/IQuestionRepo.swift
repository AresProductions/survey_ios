//
//  IQuestionRepo.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import Combine
import Foundation

protocol IQuestionRepo {
    func sync() -> AnyPublisher<Void, Error>

    func get(id: Int) -> AnyPublisher<Question?, Never>
    
    func getAll() -> AnyPublisher<[Question], Never>
}
