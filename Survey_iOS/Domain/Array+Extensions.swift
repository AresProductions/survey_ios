//
//  Array+Extensions.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 6/12/20.
//

import Foundation

extension Array {
    /// Returns the object at the current index
    /// if it does not exist then it returns nil
    ///
    /// - Parameter index: index
    subscript(safe index: Int) -> Element? {
        indices ~= index ? self[index] : nil
    }
}
