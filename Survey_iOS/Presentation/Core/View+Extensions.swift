//
//  View+Extensions.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import SwiftUI

extension View {
    /// Hides this view completely if `shouldHide` is true
    func gone(if shouldHide: Bool) -> some View {
        shouldHide ? AnyView(EmptyView()) : AnyView(self)
    }
    
    /// Adds a NavigationView with given NavigationLinks on a View
    func inNavigationViewWithLinks<T>(_ view: T) -> some View where T: View {
        NavigationView {
            ZStack {
                view
                self
            }
        }
    }

    /// Add navigation links to the given view
    private func addNavigationLinks<T>(_ view: T) -> some View where T: View {
        ZStack {
            view
            self
        }
    }

    /// Adds a NavigationView on top of the given View
    private func inNavigationView() -> some View {
        NavigationView {
            self
        }
    }
}
