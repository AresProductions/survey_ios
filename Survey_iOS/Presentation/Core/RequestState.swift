//
//  RequestState.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation

enum RequestState {
    case inProgress
    case success
    case fail
    case idle
}
