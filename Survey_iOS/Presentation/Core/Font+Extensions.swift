//
//  Font+Extensions.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 6/12/20.
//

import SwiftUI

/*
 * COPY - PASTED
 */
/// Material Design Fonts
protocol IThemeFonts {
    /// Header 4 Font
    static var h4: Font { get }

    /// Header 5 Font
    static var h5: Font { get }

    /// Header 6 Font
    static var h6: Font { get }

    /// SuThemeitle Font
    static var subtitle: Font { get }

    /// Body Font
    static var body: Font { get }

    /// Body 2 Font
    static var body2: Font { get }

    /// Button Font
    static var button: Font { get }

    /// Caption Font
    static var caption: Font { get }
}

private struct ThemeFonts: IThemeFonts {
    static var h4 = Font.system(size: 36, weight: .bold, design: .default)
    static var h5 = Font.system(size: 24, weight: .bold, design: .default)
    static var h6 = Font.system(size: 20, weight: .medium, design: .default)
    static var subtitle = Font.system(size: 16, weight: .bold, design: .default)
    static var body = Font.system(size: 16, weight: .regular, design: .default)
    static var body2 = Font.system(size: 14, weight: .regular, design: .default)
    static var button = Font.system(size: 17, weight: .medium, design: .default)
    static var caption = Font.system(size: 12, weight: .regular, design: .default)
}

extension Font {
    /// Survey Fonts
    static var theme: IThemeFonts.Type {
        return ThemeFonts.self
    }
}

#if canImport(SwiftUI) && DEBUG

struct FontDescr: Identifiable {
    var id = UUID()
    var font: Font
    var description: String
}

struct ThemeFonts_Previews: PreviewProvider {
    private static func fonts() -> [FontDescr] {
        return [(Font.theme.h4, "H4"),
                (Font.theme.h5, "H5"),
                (Font.theme.h6, "H6"),
                (Font.theme.subtitle, "Subtitle"),
                (Font.theme.body, "Body"),
                (Font.theme.body2, "Body 2"),
                (Font.theme.button, "Button"),
                (Font.theme.caption, "Caption")]
            .map { FontDescr(font: $0.0, description: $0.1) }
    }

    static var previews: some View {
        VStack {
            ForEach(fonts()) { fontDescr in
                Text(fontDescr.description)
                    .font(fontDescr.font)
                    .padding(10)
            }
        }.previewLayout(.sizeThatFits)
            .padding(20)
    }
}

#endif
