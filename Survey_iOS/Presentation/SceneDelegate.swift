//
//  SceneDelegate.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 1/12/20.
//

import SwiftUI
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        let services = AppServices.shared

        DIContainer.default = services

        let isRunningTests = NSClassFromString("XCTestCase") != nil

        if (!isRunningTests) {
            let startSurveyView = StartSurveyView(vm: StartSurveyVM())

            if let windowScene = scene as? UIWindowScene {
                let window = UIWindow(windowScene: windowScene)
                window.rootViewController = HostingController(rootView: startSurveyView)

                if let rootVC = window.rootViewController {
                    services.register(UIViewController.self, instance: rootVC)
                }

                self.window = window
                window.makeKeyAndVisible()
            }
        }
    }
}

class HostingController: UIHostingController<StartSurveyView> {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
