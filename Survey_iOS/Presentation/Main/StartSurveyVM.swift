//
//  MainVM.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Combine
import Foundation

class StartSurveyVM: ObservableObject {
    @Injected private var syncQuestionsUC: ISyncQuestionsUC

    private var cancelables = [AnyCancellable]()

    @Published var requestQuestionsState: RequestState = .idle
    @Published var stateString: String = ""
    @Published var buttonDisabled: Bool = true
    @Published var startSurveyViewDestination: StartSurveyViewDestination? = .nowhere

    init() {
        syncQuestions()
        observeRequestState()
    }

    func syncQuestions() {
        requestQuestionsState = .inProgress
        syncQuestionsUC.execute()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case let .failure(error):
                    print(error)
                    self.requestQuestionsState = .fail
                case .finished:
                    self.requestQuestionsState = .success
                }
            }, receiveValue: { _ in })
            .store(in: &cancelables)
    }

    func observeRequestState() {
        $requestQuestionsState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                switch state {
                case .success:
                    self?.stateString = "Ready!"
                    self?.buttonDisabled = false
                case .fail:
                    self?.stateString = "Failed to download questions :("
                    self?.buttonDisabled = true
                case .inProgress:
                    self?.stateString = "Downloading..."
                    self?.buttonDisabled = true
                case .idle:
                    self?.stateString = "Idle..."
                }
            }.store(in: &cancelables)
    }
}

/// Possible navigation paths for StartSurveyView
enum StartSurveyViewDestination: Int {
    case nowhere
    case answerQuestion
}
