//
//  Main.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 3/12/20.
//

import Foundation
import SwiftUI

struct StartSurveyView: View {
    @StateObject var vm: StartSurveyVM

    var body: some View {
        VStack {
            Spacer()
            Button("Start Survey", action: {
                vm.startSurveyViewDestination = .answerQuestion
            }).disabled(vm.buttonDisabled)
            Spacer()
            Button("Retry", action: {
                vm.syncQuestions()
            }).gone(if: vm.requestQuestionsState != .fail)
            Text(vm.stateString)
        }.navigationBarTitle(Text("Welcome"), displayMode: .large)
        .inNavigationViewWithLinks(navigationLinks())
    }

    private func navigationLinks() -> some View {
        // Add NavigationLinks here
        Group {
            NavigationLink(destination: AnswerQuestionView(vm: AnswerQuestionVM()),
                           tag: StartSurveyViewDestination.answerQuestion,
                           selection: $vm.startSurveyViewDestination) {
                EmptyView()
            }
        }
    }
}


#if canImport(SwiftUI) && DEBUG

struct StartSurveyView_Previews: PreviewProvider {
    static var previews: some View {
        StartSurveyView(vm: StartSurveyVM())
    }
}

#endif
