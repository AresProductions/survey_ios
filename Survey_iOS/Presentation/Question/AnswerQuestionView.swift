//
//  AnswerQuestionView.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Foundation
import SwiftUI

struct AnswerQuestionView: View {
    @StateObject var vm: AnswerQuestionVM

    var body: some View {
        VStack {
            questionsSubmitedView()
            questionView()
            if let answer = vm.currentQnA?.answer {
                answerView(answer)
            } else {
                textFieldSubmitView()
            }
            postStateView()
            Spacer()
            previousNextButtonsView()
        }.navigationBarTitle(Text("Question \(vm.currentIndex + 1)/\(vm.questionsCount)"), displayMode: .large)
    }

    private func questionsSubmitedView() -> some View {
        Text("Questions submitted: \(vm.answersCount)").padding()
            .font(Font.theme.body)
    }

    private func questionView() -> some View {
        HStack {
            Text(vm.currentQnA?.question ?? "")
                .font(Font.theme.h5)
                .padding()
            Spacer()
        }
    }

    private func textFieldSubmitView() -> some View {
        VStack {
            TextField("Add your answer here...", text: $vm.textFieldAnswer)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .font(Font.theme.body)
                .padding()
            Button("Submit") {
                vm.postAnswer()
            }
            .disabled(vm.submitButtonDisabled)
            .font(Font.theme.button)
            .padding()
        }
    }

    private func answerView(_ answer: String) -> some View {
        HStack {
            Text(answer)
                .font(Font.theme.body)
                .padding()
            Spacer()
        }
    }

    private func postStateView() -> some View {
        VStack {
            Text(vm.postAnswerStateString)
                .font(Font.theme.subtitle)
                .padding()
        }.gone(if: vm.postViewStateGone)
            .padding()
    }

    private func previousNextButtonsView() -> some View {
        HStack {
            Button("Previous", action: {
                vm.previousPage()
            })
                .font(Font.theme.button)
                .disabled(vm.previousButtonDisabled)
                .padding()

            Spacer()

            Button("Next", action: {
                vm.nextPage()
            })
                .font(Font.theme.button)
                .disabled(vm.nextButtonDisabled)
                .padding()
        }
    }
}

#if canImport(SwiftUI) && DEBUG

struct AnswerQuestionView_Previews: PreviewProvider {
    static var previews: some View {
        AnswerQuestionView(vm: AnswerQuestionVM())
    }
}

#endif
