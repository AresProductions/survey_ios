//
//  AnswerQuestionVM.swift
//  Survey_iOS
//
//  Created by Ares Ceka on 5/12/20.
//

import Combine
import Foundation

class AnswerQuestionVM: ObservableObject {
    // MARK: - DI

    @Injected private var getQuestionsWithAnswersUC: IGetQuestionsWithAnswersUC
    @Injected private var getAllAnswersUC: IGetAllAnswersUC
    @Injected private var postAnswerUC: IPostAnswerUC

    // MARK: - Public vars

    @Published var currentQnA: QuestionWithAnswer?
    @Published var currentIndex: Int = 0
    @Published var questionsCount: Int = 0
    @Published var answersCount: Int = 0
    @Published var textFieldAnswer = ""

    @Published var nextButtonDisabled = false
    @Published var previousButtonDisabled = false
    @Published var submitButtonDisabled = false

    @Published var postAnswerState: RequestState = .idle
    @Published var postViewStateGone = true
    @Published var postAnswerStateString = ""

    // MARK: - Private vars

    private var questionsWithAnswers = [QuestionWithAnswer]()
    private var cancelables = [AnyCancellable]()

    // MARK: - Init

    init() {
        observeIndex()
        observeQuestionsWithAnswers()
        observerSubmittedAnswers()
        observeTextField()
        observePostAnswerState()
    }

    // MARK: - Public Functions

    func nextPage() {
        if currentIndex < questionsCount - 1 {
            currentIndex += 1
        }
    }

    func previousPage() {
        if currentIndex > 0 {
            currentIndex -= 1
        }
    }

    func postAnswer() {
        if let currentQnA = currentQnA {
            postAnswerState = .inProgress
            postAnswerUC.execute(answer: Answer(id: currentQnA.id, description: textFieldAnswer))
                .receive(on: DispatchQueue.main)
                .sink { completion in
                    switch completion {
                    case .finished:
                        self.postAnswerState = .success
                    case let .failure(error):
                        print(error)
                        self.postAnswerState = .fail
                    }
                } receiveValue: { _ in }
                .store(in: &cancelables)
        }
    }

    // MARK: - Private Functions

    private func observeQuestionsWithAnswers() {
        getQuestionsWithAnswersUC.execute()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] questionsWithAnswers in
                self?.questionsCount = questionsWithAnswers.count
                self?.questionsWithAnswers = questionsWithAnswers
                self?.resetCurrentView(newIndex: self?.currentIndex ?? 0)
            }.store(in: &cancelables)
    }

    private func observerSubmittedAnswers() {
        getAllAnswersUC.execute()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] answers in
                self?.answersCount = answers.count
            }.store(in: &cancelables)
    }

    private func observeIndex() {
        $currentIndex
            .receive(on: DispatchQueue.main)
            .sink { [weak self] newIndex in
                self?.postAnswerState = .idle
                self?.resetCurrentView(newIndex: newIndex)
            }.store(in: &cancelables)
    }

    private func resetCurrentView(newIndex: Int) {
        currentQnA = questionsWithAnswers[safe: newIndex]
        textFieldAnswer = ""
        if newIndex <= 0 {
            previousButtonDisabled = true
        } else {
            previousButtonDisabled = false
        }

        if newIndex >= questionsWithAnswers.count - 1 {
            nextButtonDisabled = true
        } else {
            nextButtonDisabled = false
        }
    }

    private func observeTextField() {
        $textFieldAnswer
            .receive(on: DispatchQueue.main)
            .sink { [weak self] text in
                if text.isEmpty {
                    self?.submitButtonDisabled = true
                } else {
                    self?.submitButtonDisabled = false
                }
            }.store(in: &cancelables)
    }

    private func observePostAnswerState() {
        $postAnswerState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                switch self?.postAnswerState {
                case .success:
                    self?.postAnswerStateString = "Success!"
                    self?.postViewStateGone = false
                case .fail:
                    self?.postAnswerStateString = "Failed to submit your answer :(\nPlease try again..."
                    self?.postViewStateGone = false

                case .inProgress:
                    self?.postAnswerStateString = "Submitting..."
                    self?.postViewStateGone = false
                default:
                    self?.postAnswerStateString = "Idle"
                    self?.postViewStateGone = true
                }
            }.store(in: &cancelables)
    }
}
