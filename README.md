# Survey App iOS

### Technologies
* Language Used: Swift
* Libraries: SwiftUI, Core Data, Combine
* iOS Target: 14+

### Architecture
* General Architecture: Clean Architecture
* Data Layer: Remote Source saves data on the Database which is the Single Source of Truth for the whole project
* Presentation Layer: MVVM
* Testing: Only Unit Testing
